EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:1-179277-5
LIBS:1N4148WS-7-F
LIBS:1N5817
LIBS:1SMB5925BT3G
LIBS:1SMB5936BT3G
LIBS:3SMAJ5919B-TP
LIBS:74xgxx
LIBS:0805B105K160CT
LIBS:1591-7020
LIBS:3350-4275-126
LIBS:3350-4275-246
LIBS:06033C223KAT2A
LIBS:7090.9010.03
LIBS:68001-202HLF
LIBS:70553-0038
LIBS:150060RS75000
LIBS:0154010.DR
LIBS:744227
LIBS:ABLS-8.000MHZ-B4-T
LIBS:ABM7-8.000MHZ-D2Y-T
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:BC848B_235
LIBS:BLM18KG471SH1D
LIBS:bosch
LIBS:brooktre
LIBS:BSS83P_H6327
LIBS:C0402C103J5RACTU
LIBS:C0402C222K5RACTU
LIBS:C0402C223K4RACTU
LIBS:C0603C102K1GECTU
LIBS:C0603C104K4RACTU
LIBS:C0603C105Z8VACTU
LIBS:C0603C200K4GAC7867
LIBS:C0603C223K5RACTU
LIBS:C0603C569D5GACTU
LIBS:C0805C106K8PACTU
LIBS:CBR06C101F5GAC
LIBS:CGA3E1X7R1C105K080AC
LIBS:CGA4J3X5R1A106K125AB
LIBS:cmos_ieee
LIBS:CRCW12061K00JNEAHP
LIBS:dc-dc
LIBS:diode
LIBS:EEEFK1V471SP
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:FC-135_32.7680KA-AC
LIBS:ftdi
LIBS:gennum
LIBS:graphic_symbols
LIBS:hc11
LIBS:HEF4027BT
LIBS:infineon
LIBS:intersil
LIBS:ir
LIBS:L78L05ACD13TR
LIBS:L78S05CV-DG
LIBS:L78S12CV
LIBS:Lattice
LIBS:leds
LIBS:LEM
LIBS:LM358AMX
LIBS:LM393AD
LIBS:logic_programmable
LIBS:LTST-C191KGKT
LIBS:LTST-C191KRKT
LIBS:maxim
LIBS:MC-146_32.7680KA-A0_ROHS
LIBS:MCMR06X103_JTL
LIBS:MCP2551-I_SN
LIBS:MCSR04X1004FTL
LIBS:MCSR06X39R0FTL
LIBS:MCSR06X1003FTL
LIBS:mechanical
LIBS:MF50_100R
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:modules
LIBS:motor_drivers
LIBS:MPU-6050
LIBS:msp430
LIBS:MX23A18NF1
LIBS:MX34003NF1
LIBS:MX34005NF1
LIBS:MX34032NF2
LIBS:MX44006NF1
LIBS:N2540-6002RB
LIBS:NC7SZ32M5X
LIBS:nordicsemi
LIBS:nxp
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:PE-1812ACC110STS
LIBS:Power_Management
LIBS:powerint
LIBS:pspice
LIBS:R-78E5.0-0.5
LIBS:RC0402FR-071ML
LIBS:RC0603FR-071ML
LIBS:RC0603FR-075K6L
LIBS:RC0603FR-07120RL
LIBS:RC0603JR-071KL
LIBS:RC0603JR-134K7L
LIBS:RC0603JR-0710KL
LIBS:RC0603JR-0722RL
LIBS:RC0603JR-0768RL
LIBS:RC0603JR-07510RL
LIBS:references
LIBS:rfcom
LIBS:RFSolutions
LIBS:RT9193-33GB
LIBS:S24SE05004NDFA
LIBS:S2008LS2
LIBS:sensors
LIBS:silabs
LIBS:SML-D13DWT86A
LIBS:SML-D13FWT86
LIBS:SN74AHC1G04DBVT
LIBS:SN74AHCT1G08DBVR
LIBS:SN74LVC2G32DCUR
LIBS:stm8
LIBS:stm32
LIBS:STM32F103C8T6
LIBS:STM32F103C8T6TR
LIBS:supertex
LIBS:SZNUP2105LT3G
LIBS:transf
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:BC847_235
LIBS:RC0603JR-07220RL
LIBS:BSPD1_V2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "BSPD1_V2"
Date "2018-08-14"
Rev "V1"
Comp "Cure Mannheim e.V."
Comment1 "Autor: Lukas Neumann"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R-78E5.0-0.5 U1
U 1 1 5B732A62
P 1350 1650
F 0 "U1" H 1900 1350 50  0000 L CNN
F 1 "R-78E5.0-0.5" H 1700 1850 50  0000 L CNN
F 2 "SHDRV3W100P0_254_1X3_1160X850X1040P" H 2400 1750 50  0001 L CNN
F 3 "http://docs-europe.electrocomponents.com/webdocs/10a2/0900766b810a2db7.pdf" H 2400 1650 50  0001 L CNN
F 4 "Switching Regulator,7-28Vin,5Vout 0.5A Switching Regulator, 7" H 2400 1550 50  0001 L CNN "Description"
F 5 "" H 2400 1450 50  0001 L CNN "Height"
F 6 "RECOM Power" H 2400 1350 50  0001 L CNN "Manufacturer_Name"
F 7 "R-78E5.0-0.5" H 2400 1250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "1666675" H 2400 1150 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/1666675" H 2400 1050 50  0001 L CNN "RS Price/Stock"
F 10 "R-78E5.0-0.5" H 2400 950 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/r-78e5.0-0.5/recom-power" H 2400 850 50  0001 L CNN "Arrow Price/Stock"
	1    1350 1650
	1    0    0    -1  
$EndComp
$Comp
L C0603C105Z8VACTU C1
U 1 1 5B732B87
P 1000 1950
F 0 "C1" H 1250 1800 50  0000 L CNN
F 1 "C0603C105Z8VACTU" H 1000 2150 50  0000 L CNN
F 2 "CAPC1608X90N" H 1350 2000 50  0001 L CNN
F 3 "https://search.kemet.com/component-edge/download/specsheet/C0603C105Z8VACTU.pdf" H 1350 1900 50  0001 L CNN
F 4 "KEMET - C0603C105Z8VACTU - CAP, MLCC, Y5V, 1UF, 10V, 0603" H 1350 1800 50  0001 L CNN "Description"
F 5 "0.9" H 1350 1700 50  0001 L CNN "Height"
F 6 "Kemet" H 1350 1600 50  0001 L CNN "Manufacturer_Name"
F 7 "C0603C105Z8VACTU" H 1350 1500 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 1350 1400 50  0001 L CNN "RS Part Number"
F 9 "" H 1350 1300 50  0001 L CNN "RS Price/Stock"
F 10 "70096976" H 1350 1200 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/kemet-c0603c105z8vactu/70096976/" H 1350 1100 50  0001 L CNN "Allied Price/Stock"
F 12 "C0603C105Z8VACTU" H 1350 1000 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/c0603c105z8vactu/kemet-corporation" H 1350 900 50  0001 L CNN "Arrow Price/Stock"
	1    1000 1950
	0    -1   -1   0   
$EndComp
$Comp
L +24V #PWR01
U 1 1 5B72D9A7
P 1000 1350
F 0 "#PWR01" H 1000 1200 50  0001 C CNN
F 1 "+24V" H 1000 1490 50  0000 C CNN
F 2 "" H 1000 1350 50  0001 C CNN
F 3 "" H 1000 1350 50  0001 C CNN
	1    1000 1350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5B72D9C1
P 1000 2050
F 0 "#PWR02" H 1000 1800 50  0001 C CNN
F 1 "GND" H 1000 1900 50  0000 C CNN
F 2 "" H 1000 2050 50  0001 C CNN
F 3 "" H 1000 2050 50  0001 C CNN
	1    1000 2050
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR03
U 1 1 5B72D9DB
P 2650 1650
F 0 "#PWR03" H 2650 1500 50  0001 C CNN
F 1 "+5V" H 2650 1790 50  0000 C CNN
F 2 "" H 2650 1650 50  0001 C CNN
F 3 "" H 2650 1650 50  0001 C CNN
	1    2650 1650
	1    0    0    -1  
$EndComp
$Comp
L LM393AD IC2
U 1 1 5B72DF1D
P 4650 4000
F 0 "IC2" H 5150 3550 50  0000 L CNN
F 1 "LM393AD" H 5100 4150 50  0000 L CNN
F 2 "SOIC127P600X175-8N" H 5600 4100 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393a.pdf" H 5600 4000 50  0001 L CNN
F 4 "LM393ADG4, Dual Comparator Open Collector 1.3s 2-kanalers 3, 5, 9, 12, 15, 18, 24, 28V 8-Pin SOIC" H 5600 3900 50  0001 L CNN "Description"
F 5 "1.75" H 5600 3800 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 5600 3700 50  0001 L CNN "Manufacturer_Name"
F 7 "LM393AD" H 5600 3600 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "6612810P" H 5600 3500 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/6612810P" H 5600 3400 50  0001 L CNN "RS Price/Stock"
F 10 "LM393AD" H 5600 3300 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/lm393ad/texas-instruments" H 5600 3200 50  0001 L CNN "Arrow Price/Stock"
	1    4650 4000
	1    0    0    -1  
$EndComp
Text GLabel 4650 4100 0    51   Input ~ 0
Brakepoti
Text GLabel 4650 4000 0    51   Input ~ 0
Brakeactive(inverted)
Text Notes 7050 6350 0    118  ~ 0
Brakeactive is low, when brake is actuated.
$Comp
L GND #PWR04
U 1 1 5B72E2BC
P 4650 5050
F 0 "#PWR04" H 4650 4800 50  0001 C CNN
F 1 "GND" H 4650 4900 50  0000 C CNN
F 2 "" H 4650 5050 50  0001 C CNN
F 3 "" H 4650 5050 50  0001 C CNN
	1    4650 5050
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR05
U 1 1 5B72E4C4
P 5850 3900
F 0 "#PWR05" H 5850 3750 50  0001 C CNN
F 1 "+5V" H 5850 4040 50  0000 C CNN
F 2 "" H 5850 3900 50  0001 C CNN
F 3 "" H 5850 3900 50  0001 C CNN
	1    5850 3900
	1    0    0    -1  
$EndComp
$Comp
L SML-D13FWT86 LED1
U 1 1 5B72E518
P 1400 4850
F 0 "LED1" H 1650 4650 50  0000 L BNN
F 1 "SML-D13FWT86" H 1450 5100 50  0000 L BNN
F 2 "SMLD15YWT86" H 1900 5000 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 1900 4900 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 1900 4800 50  0001 L BNN "Description"
F 5 "" H 1900 4700 50  0001 L BNN "Height"
F 6 "ROHM Semiconductor" H 1900 4600 50  0001 L BNN "Manufacturer_Name"
F 7 "SML-D13FWT86" H 1900 4500 50  0001 L BNN "Manufacturer_Part_Number"
F 8 "1332874P" H 1900 4400 50  0001 L BNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/1332874P" H 1900 4300 50  0001 L BNN "RS Price/Stock"
F 10 "SML-D13FWT86" H 1900 4200 50  0001 L BNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/1332874P" H 1900 4100 50  0001 L BNN "Arrow Price/Stock"
	1    1400 4850
	0    -1   -1   0   
$EndComp
$Comp
L SML-D13DWT86A LED2
U 1 1 5B72E76A
P 2150 4850
F 0 "LED2" H 2400 4650 50  0000 L BNN
F 1 "SML-D13DWT86A" H 2150 5100 50  0000 L BNN
F 2 "LEDC1608X65N" H 2650 5000 50  0001 L BNN
F 3 "https://docs-emea.rs-online.com/webdocs/156b/0900766b8156b2c9.pdf" H 2650 4900 50  0001 L BNN
F 4 "ROHM SML-D13DWT86A, SML 608 nm Orange LED, 1608 (0603) Milky White SMD package" H 2650 4800 50  0001 L BNN "Description"
F 5 "0.65" H 2650 4700 50  0001 L BNN "Height"
F 6 "ROHM Semiconductor" H 2650 4600 50  0001 L BNN "Manufacturer_Name"
F 7 "SML-D13DWT86A" H 2650 4500 50  0001 L BNN "Manufacturer_Part_Number"
F 8 "1332873P" H 2650 4400 50  0001 L BNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/1332873P" H 2650 4300 50  0001 L BNN "RS Price/Stock"
F 10 "SML-D13DWT86A" H 2650 4200 50  0001 L BNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/1332873P" H 2650 4100 50  0001 L BNN "Arrow Price/Stock"
	1    2150 4850
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR06
U 1 1 5B72EB11
P 1400 5700
F 0 "#PWR06" H 1400 5450 50  0001 C CNN
F 1 "GND" H 1400 5550 50  0000 C CNN
F 2 "" H 1400 5700 50  0001 C CNN
F 3 "" H 1400 5700 50  0001 C CNN
	1    1400 5700
	1    0    0    -1  
$EndComp
Text GLabel 2150 5700 3    51   Input ~ 0
Brakeactive(inverted)
$Comp
L BC847,235 Q1
U 1 1 5B72F1D7
P 5150 6400
F 0 "Q1" H 5600 6450 50  0000 L CNN
F 1 "BC847,235" H 5600 6350 50  0000 L CNN
F 2 "SOT95P230X110-3N" H 5600 6250 50  0001 L CNN
F 3 "http://assets.nexperia.com/documents/data-sheet/BC847_SER.pdf" H 5600 6150 50  0001 L CNN
F 4 "BC847 series - 45 V, 100 mA NPN general-purpose transistors" H 5600 6050 50  0001 L CNN "Description"
F 5 "1.1" H 5600 5950 50  0001 L CNN "Height"
F 6 "Nexperia" H 5600 5850 50  0001 L CNN "Manufacturer_Name"
F 7 "BC847,235" H 5600 5750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 5600 5650 50  0001 L CNN "RS Part Number"
F 9 "" H 5600 5550 50  0001 L CNN "RS Price/Stock"
F 10 "BC847,235" H 5600 5450 50  0001 L CNN "Arrow Part Number"
F 11 "http://www.arrow.com/en/products/bc847235/nexperia" H 5600 5350 50  0001 L CNN "Arrow Price/Stock"
	1    5150 6400
	1    0    0    -1  
$EndComp
Text GLabel 4950 6400 0    51   Input ~ 0
Brakeactive(inverted)
$Comp
L GND #PWR07
U 1 1 5B72F2B2
P 5050 7400
F 0 "#PWR07" H 5050 7150 50  0001 C CNN
F 1 "GND" H 5050 7250 50  0000 C CNN
F 2 "" H 5050 7400 50  0001 C CNN
F 3 "" H 5050 7400 50  0001 C CNN
	1    5050 7400
	1    0    0    -1  
$EndComp
$Comp
L RC0603JR-0710KL R5
U 1 1 5B72F3DD
P 5050 7200
F 0 "R5" H 5400 7300 50  0000 C CNN
F 1 "RC0603JR-0710KL" H 5400 7100 50  0000 C CNN
F 2 "RESC1608X55N" H 5400 7000 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC0603_51_RoHS_L_v5.pdf" H 5400 6900 50  0001 C CNN
F 4 "YAGEO (PHYCOMP) - RC0603JR-0710KL - RES, THICK FILM, 10K, 5%, 0.1W, 0603" H 5400 6800 50  0001 C CNN "Description"
F 5 "RS" H 5400 6700 50  0001 C CNN "Supplier_Name"
F 6 "" H 5400 6600 50  0001 C CNN "RS Part Number"
F 7 "YAGEO (PHYCOMP)" H 5400 6500 50  0001 C CNN "Manufacturer_Name"
F 8 "RC0603JR-0710KL" H 5400 6400 50  0001 C CNN "Manufacturer_Part_Number"
F 9 "" H 5400 6300 50  0001 C CNN "Allied_Number"
F 10 "" H 5400 6200 50  0001 C CNN "Other Part Number"
F 11 "0.55" H 5400 6100 50  0001 C CNN "Height"
	1    5050 7200
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR08
U 1 1 5B72F706
P 5450 6050
F 0 "#PWR08" H 5450 5900 50  0001 C CNN
F 1 "+5V" H 5450 6190 50  0000 C CNN
F 2 "" H 5450 6050 50  0001 C CNN
F 3 "" H 5450 6050 50  0001 C CNN
	1    5450 6050
	1    0    0    -1  
$EndComp
Text Notes 2600 850  0    118  ~ 0
Power Supply
Wire Wire Line
	1000 1350 1000 1450
Wire Wire Line
	1000 1950 1000 2050
Wire Wire Line
	1350 1750 1200 1750
Wire Wire Line
	1200 1750 1200 2000
Wire Wire Line
	1200 2000 1000 2000
Connection ~ 1000 2000
Wire Wire Line
	1350 1650 1200 1650
Wire Wire Line
	1200 1650 1200 1400
Wire Wire Line
	1200 1400 1000 1400
Connection ~ 1000 1400
Wire Wire Line
	2550 1650 2650 1650
Wire Wire Line
	4650 4300 4650 5050
Wire Wire Line
	4400 4200 4650 4200
Wire Wire Line
	4450 4250 4450 4200
Connection ~ 4450 4200
Wire Wire Line
	1400 5650 1400 5700
Wire Wire Line
	2150 5650 2150 5700
Wire Wire Line
	1400 4850 1400 4950
Wire Wire Line
	2150 4850 2150 4950
Wire Wire Line
	1400 4200 1400 4250
Wire Wire Line
	2150 4250 2150 4200
Wire Wire Line
	4950 6400 5150 6400
Wire Wire Line
	5050 6400 5050 6500
Connection ~ 5050 6400
Wire Wire Line
	5050 7200 5050 7400
Wire Wire Line
	5450 6700 5450 6850
Wire Wire Line
	5450 6050 5450 6100
Wire Notes Line
	600  2900 6000 2900
Wire Notes Line
	3250 2900 3250 7650
Wire Notes Line
	3250 7650 3300 7650
Wire Notes Line
	6950 6150 6950 600 
Wire Notes Line
	5950 2900 6950 2900
Wire Notes Line
	3250 5250 6950 5250
Text Notes 1550 3400 0    118  ~ 0
LEDs
Text Notes 4500 3350 0    118  ~ 0
Comparator
Text Notes 4750 5600 0    118  ~ 0
Output
Text Notes 8550 1100 0    118  ~ 0
Connectors
Text GLabel 5450 6850 3    51   Input ~ 0
Output(BSPD1)
Text GLabel 8450 1800 0    51   Input ~ 0
Output(BSPD1)
$Comp
L +24V #PWR09
U 1 1 5B7303A4
P 8450 1650
F 0 "#PWR09" H 8450 1500 50  0001 C CNN
F 1 "+24V" H 8450 1790 50  0000 C CNN
F 2 "" H 8450 1650 50  0001 C CNN
F 3 "" H 8450 1650 50  0001 C CNN
	1    8450 1650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5B7303DC
P 8450 2100
F 0 "#PWR010" H 8450 1850 50  0001 C CNN
F 1 "GND" H 8450 1950 50  0000 C CNN
F 2 "" H 8450 2100 50  0001 C CNN
F 3 "" H 8450 2100 50  0001 C CNN
	1    8450 2100
	1    0    0    -1  
$EndComp
Text GLabel 8450 1950 0    51   Input ~ 0
Brakepoti
$Comp
L C0603C105Z8VACTU C5
U 1 1 5B73065B
P 5950 4000
F 0 "C5" H 6200 3850 50  0000 L CNN
F 1 "C0603C105Z8VACTU" H 5950 4200 50  0000 L CNN
F 2 "CAPC1608X90N" H 6300 4050 50  0001 L CNN
F 3 "https://search.kemet.com/component-edge/download/specsheet/C0603C105Z8VACTU.pdf" H 6300 3950 50  0001 L CNN
F 4 "KEMET - C0603C105Z8VACTU - CAP, MLCC, Y5V, 1UF, 10V, 0603" H 6300 3850 50  0001 L CNN "Description"
F 5 "0.9" H 6300 3750 50  0001 L CNN "Height"
F 6 "Kemet" H 6300 3650 50  0001 L CNN "Manufacturer_Name"
F 7 "C0603C105Z8VACTU" H 6300 3550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 6300 3450 50  0001 L CNN "RS Part Number"
F 9 "" H 6300 3350 50  0001 L CNN "RS Price/Stock"
F 10 "70096976" H 6300 3250 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/kemet-c0603c105z8vactu/70096976/" H 6300 3150 50  0001 L CNN "Allied Price/Stock"
F 12 "C0603C105Z8VACTU" H 6300 3050 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/c0603c105z8vactu/kemet-corporation" H 6300 2950 50  0001 L CNN "Arrow Price/Stock"
	1    5950 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 4000 5950 4000
Wire Wire Line
	5850 3900 5850 4000
Connection ~ 5850 4000
$Comp
L GND #PWR011
U 1 1 5B7307E3
P 6600 4000
F 0 "#PWR011" H 6600 3750 50  0001 C CNN
F 1 "GND" H 6600 3850 50  0000 C CNN
F 2 "" H 6600 4000 50  0001 C CNN
F 3 "" H 6600 4000 50  0001 C CNN
	1    6600 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 4000 6600 4000
$Comp
L RC0603JR-07220RL R1
U 1 1 5B745333
P 1400 5650
F 0 "R1" H 1700 5550 50  0000 L CNN
F 1 "RC0603JR-07220RL" H 1300 5800 50  0000 L CNN
F 2 "RESC1608X55N" H 1950 5700 50  0001 L CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC0603_51_RoHS_L_v5.pdf" H 1950 5600 50  0001 L CNN
F 4 "YAGEO RC0603 VALUE1" H 1950 5500 50  0001 L CNN "Description"
F 5 "0.55" H 1950 5400 50  0001 L CNN "Height"
F 6 "YAGEO (PHYCOMP)" H 1950 5300 50  0001 L CNN "Manufacturer_Name"
F 7 "RC0603JR-07220RL" H 1950 5200 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 1950 5100 50  0001 L CNN "RS Part Number"
F 9 "" H 1950 5000 50  0001 L CNN "RS Price/Stock"
F 10 "RC0603JR-07220RL" H 1950 4900 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/rc0603jr-07220rl/yageo" H 1950 4800 50  0001 L CNN "Arrow Price/Stock"
	1    1400 5650
	0    -1   -1   0   
$EndComp
$Comp
L RC0603JR-07220RL R2
U 1 1 5B7454AF
P 2150 5650
F 0 "R2" H 2450 5550 50  0000 L CNN
F 1 "RC0603JR-07220RL" H 2050 5800 50  0000 L CNN
F 2 "RESC1608X55N" H 2700 5700 50  0001 L CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC0603_51_RoHS_L_v5.pdf" H 2700 5600 50  0001 L CNN
F 4 "YAGEO RC0603 VALUE1" H 2700 5500 50  0001 L CNN "Description"
F 5 "0.55" H 2700 5400 50  0001 L CNN "Height"
F 6 "YAGEO (PHYCOMP)" H 2700 5300 50  0001 L CNN "Manufacturer_Name"
F 7 "RC0603JR-07220RL" H 2700 5200 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 2700 5100 50  0001 L CNN "RS Part Number"
F 9 "" H 2700 5000 50  0001 L CNN "RS Price/Stock"
F 10 "RC0603JR-07220RL" H 2700 4900 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/rc0603jr-07220rl/yageo" H 2700 4800 50  0001 L CNN "Arrow Price/Stock"
	1    2150 5650
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR012
U 1 1 5B745660
P 1400 4200
F 0 "#PWR012" H 1400 4050 50  0001 C CNN
F 1 "+5V" H 1400 4340 50  0000 C CNN
F 2 "" H 1400 4200 50  0001 C CNN
F 3 "" H 1400 4200 50  0001 C CNN
	1    1400 4200
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR013
U 1 1 5B74568F
P 2150 4200
F 0 "#PWR013" H 2150 4050 50  0001 C CNN
F 1 "+5V" H 2150 4340 50  0000 C CNN
F 2 "" H 2150 4200 50  0001 C CNN
F 3 "" H 2150 4200 50  0001 C CNN
	1    2150 4200
	1    0    0    -1  
$EndComp
$Comp
L RC0603JR-071KL R3
U 1 1 5B745832
P 3700 4200
F 0 "R3" H 4050 4300 50  0000 C CNN
F 1 "RC0603JR-071KL" H 4050 4100 50  0000 C CNN
F 2 "RESC1608X55N" H 4050 4000 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC0603_51_RoHS_L_v5.pdf" H 4050 3900 50  0001 C CNN
F 4 "YAGEO (PHYCOMP) - RC0603JR-071KL - RES, THICK FILM, 1K, 5%, 0.1W, 0603" H 4050 3800 50  0001 C CNN "Description"
F 5 "RS" H 4050 3700 50  0001 C CNN "Supplier_Name"
F 6 "" H 4050 3600 50  0001 C CNN "RS Part Number"
F 7 "YAGEO (PHYCOMP)" H 4050 3500 50  0001 C CNN "Manufacturer_Name"
F 8 "RC0603JR-071KL" H 4050 3400 50  0001 C CNN "Manufacturer_Part_Number"
F 9 "" H 4050 3300 50  0001 C CNN "Allied_Number"
F 10 "" H 4050 3200 50  0001 C CNN "Other Part Number"
F 11 "0.55" H 4050 3100 50  0001 C CNN "Height"
	1    3700 4200
	1    0    0    -1  
$EndComp
$Comp
L RC0603FR-075K6L R4
U 1 1 5B74589B
P 4450 4250
F 0 "R4" H 4750 4150 50  0000 L CNN
F 1 "RC0603FR-075K6L" H 4450 4350 50  0000 L CNN
F 2 "RESC1608X55N" H 5000 4300 50  0001 L CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC0603_51_RoHS_L_v5.pdf" H 5000 4200 50  0001 L CNN
F 4 "YAGEO (PHYCOMP) - RC0603FR-075K6L - RES, THICK FILM, 5K6, 1%, 0.1W, 0603" H 5000 4100 50  0001 L CNN "Description"
F 5 "YAGEO (PHYCOMP)" H 5000 4000 50  0001 L CNN "Manufacturer_Name"
F 6 "RC0603FR-075K6L" H 5000 3900 50  0001 L CNN "Manufacturer_Part_Number"
F 7 "RS" H 5000 3800 50  0001 L CNN "Supplier_Name"
F 8 "" H 5000 3700 50  0001 L CNN "RS Part Number"
F 9 "0.55" H 5000 3600 50  0001 L CNN "Height"
	1    4450 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	4650 5000 4450 5000
Wire Wire Line
	4450 5000 4450 4950
Connection ~ 4650 5000
$Comp
L +5V #PWR014
U 1 1 5B745A0E
P 3700 4200
F 0 "#PWR014" H 3700 4050 50  0001 C CNN
F 1 "+5V" H 3700 4340 50  0000 C CNN
F 2 "" H 3700 4200 50  0001 C CNN
F 3 "" H 3700 4200 50  0001 C CNN
	1    3700 4200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
